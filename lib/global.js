"use strict";

module.exports = {
  jsonResponse: (
    response,
    serializedData,
    status,
    paginated = false,
    query = {},
    extraData = {}
  ) => {
    let data = {};
    let meta = {
      query,
      status,
    };

    if (!paginated) {
      data = serializedData;
    } else {
      let jsonData = serializedData.toJSON();
      data = jsonData.data;
      meta = {
        query,
        total: jsonData.total,
        perPage: jsonData.perPage,
        page: jsonData.page,
        lastPage: jsonData.lastPage,
        status,
      };
    }

    return response.status(status).json({
      ...extraData,
      data,
      meta,
    });
  },
};
