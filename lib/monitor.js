#!/usr/bin/env node
const { Cam } = require("onvif");
const fs = require("fs");
const request = require("request");
const moment = require("moment");
const Env = use("Env");
const IpCamera = use("App/Models/IpCamera");
const Event = use("Event");
const Helpers = use("Helpers");

let MotionTopic = {
  CELL_MOTION_DETECTOR: "CELL_MOTION_DETECTOR",
  MOTION_ALARM: "MOTION_ALARM",
};

class Monitor {
  constructor(id, name, onvifCam) {
    this.id = id;
    this.name = name;
    this.onvifCam = onvifCam;
    this.lastMotionDetectedState = null;
    this.topic = MotionTopic.MOTION_ALARM;
  }

  log(msg, ...rest) {
    console.log(`[camera ${this.name}]: ${msg}`, ...rest);
  }

  async start() {
    try {
      this.onvifCam.on("event", (camMessage) => {
        this.onEventReceived(camMessage);
      });
    } catch (e) {
      this.onvifCam = null;
      this.removeCamera();
      console.log(e.message);
    }
    this.log("Started");
  }

  onEventReceived(camMessage) {
    const topic = camMessage.topic._;
    if (topic.match(/RuleEngine\/CellMotionDetector\/Motion$/)) {
      this.onMotionDetectedEvent(camMessage);
    }
  }

  async removeCamera() {
    let camera = await IpCamera.find(this.id);
    await camera.delete();
    this.log("Removed");
  }

  onMotionDetectedEvent(camMessage) {
    const isMotion = camMessage.message.message.data.simpleItem.$.Value;
    if (this.lastMotionDetectedState !== isMotion) {
      this.log(`CellMotionDetector: Motion Detected: ${isMotion}`);
      this.onvifCam.getSnapshotUri({}, (err, result) => {
        if (isMotion) {
          this.fetchSnapshot(err, result);
        }
      });
    }
    this.lastMotionDetectedState = isMotion;
  }

  fetchSnapshot(err, result) {
    let snapUrl = result["uri"];
    request.head(snapUrl, (err, res, body) => {
      let counter = 0;
      let time = moment();
      let dir = `snapshots/${this.id}/${time.format("Y-MM-DD")}`;

      if (!fs.existsSync(Helpers.publicPath(dir))) {
        fs.mkdirSync(Helpers.publicPath(dir), { recursive: true });
      }

      let filenameBase = `${dir}/snapshot-${this.id}-${time.format("x")}`;

      request(snapUrl).pipe(
        fs.createWriteStream(
          Helpers.publicPath(`${filenameBase}-${counter}.jpeg`)
        )
      );
      counter++;

      let interval500 = setInterval(() => {
        request(snapUrl).pipe(
          fs.createWriteStream(
            Helpers.publicPath(`${filenameBase}-${counter}.jpeg`)
          )
        );
        counter++;

        if (counter == Env.get("SNAPSHOT_LIMIT")) {
          clearInterval(interval500);
          Event.fire("motion::save", {
            ip_camera_id: this.id,
            timestamp: time.format("x"),
            images: filenameBase,
          });
        }
      }, 1000);
    });
  }

  static createCamera(conf) {
    return new Promise((resolve) => {
      const cam = new Cam(conf, () => resolve(cam));
    });
  }

  static async create({ id, hostname, username, password, port, name }) {
    const cam = await this.createCamera({
      hostname,
      username,
      password,
      port,
    });
    return new Monitor(id, name, cam);
  }
}

module.exports = Monitor;
