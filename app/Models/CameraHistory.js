"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class CameraHistory extends Model {
  getImages(val) {
    return JSON.parse(val);
  }

  ip_camera() {
    return this.belongsTo("App/Models/IpCamera");
  }
}

module.exports = CameraHistory;
