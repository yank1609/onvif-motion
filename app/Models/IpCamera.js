"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class IpCamera extends Model {
  camera_histories() {
    return this.hasMany("App/Models/CameraHistory");
  }
}

module.exports = IpCamera;
