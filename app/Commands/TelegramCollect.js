"use strict";

const { Command } = require("@adonisjs/ace");
const axios = require("axios");
const Env = use("Env");
const Telegram = use("App/Models/Telegram");

class TelegramCollect extends Command {
  static get signature() {
    return "telegram:collect";
  }

  static get description() {
    return "Tell something helpful about this command";
  }

  async handle(args, options) {
    let ids = await Telegram.ids();
    let response = await axios.get(
      `https://api.telegram.org/bot${Env.get("TELEGRAM_TOKEN")}/getUpdates`
    );

    let results = [];
    let data = response.data;
    if (data.ok) {
      data.result.forEach((r) => {
        if (r.message) {
          if (r.message.text === "/start") {
            let name = `${r.message.from.first_name}`;

            if (r.message.from.last_name) {
              name += ` ${r.message.from.last_name}`;
            }

            results.push({
              id: r.message.from.id,
              name,
            });
          }
        }
      });
    }

    if (results.length) {
      results = results.filter(
        (v, i, a) => a.findIndex((t) => t.id === v.id) === i
      );
    }

    results = results.filter((r) => ids.indexOf(r.id) === -1);

    if (results.length) {
      for (let i = 0; i < results.length; i++) {
        await Telegram.create(results[i]);
      }

      this.info(`Saving ${results.length} new user to database`);
    } else {
      this.info(`No new user exist`);
    }

    process.exit(1);
  }
}

module.exports = TelegramCollect;
