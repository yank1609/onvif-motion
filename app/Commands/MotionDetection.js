"use strict";

const { Command } = require("@adonisjs/ace");
const Monitor = use("Lib/monitor");
const IpCamera = use("App/Models/IpCamera");

class MotionDetection extends Command {
  static get signature() {
    return "motion:detection";
  }

  static get description() {
    return "Run Motion Detection Script";
  }

  async handle() {
    async function start(args) {
      const monitor = await Monitor.create({
        id: args.id,
        name: args.name,
        hostname: args.hostname,
        username: args.username,
        password: args.password,
        port: args.port,
      });
      monitor.start();
    }

    async function main() {
      let cameras = (await IpCamera.all()).toJSON();
      cameras.forEach((cam) => {
        start({
          id: cam.id,
          name: cam.name,
          hostname: cam.hostname,
          username: cam.username,
          password: cam.password,
          port: cam.port,
        });
      });
    }

    main();
  }
}

module.exports = MotionDetection;
