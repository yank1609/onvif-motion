"use strict";
const globalHelper = use("Lib/global");
const StakeholderEmail = use("App/Models/StakeholderEmail");

class StakeholderEmailController {
  async index({ response, request, view }) {
    let emails = await StakeholderEmail.all();

    return view.render("stakeholder_emails.index", {
      emails: emails.toJSON(),
    });
  }

  async create({ response, request }) {
    let data = request.only(["name", "email"]);
    let email = await StakeholderEmail.create(data);
    return globalHelper.jsonResponse(response, email, 201);
  }

  async delete({ params, response, request }) {
    let email = await StakeholderEmail.find(params.id);
    await email.delete();
    return globalHelper.jsonResponse(
      response,
      {
        message: "Successfully deleting record",
      },
      200
    );
  }
}

module.exports = StakeholderEmailController;
