"use strict";
const onvif = require("onvif");
const globalHelper = use("Lib/global");
const IpCamera = use("App/Models/IpCamera");

class IpCameraController {
  async index({ response, request, view }) {
    return view.render("ip_camera.index");
  }

  async create({ response, request }) {
    let data = request.only([
      "name",
      "hostname",
      "port",
      "username",
      "password",
    ]);

    let ip_cam = await IpCamera.create(data);
    return globalHelper.jsonResponse(response, ip_cam, 201);
  }

  async probe({ response, request }) {
    let ip_cameras = (await IpCamera.all()).toJSON();
    let cams = await this.probe_camera();

    if (ip_cameras.length > 0) {
      ip_cameras.forEach((camera) => {
        let exist = cams.findIndex((o) => o.hostname === camera.hostname);

        if (exist > -1) {
          cams.splice(exist, 1);
        }

        cams.push({
          id: camera.id,
          name: camera.name,
          hostname: camera.hostname,
          username: camera.username,
          password: camera.password,
          port: camera.port,
        });
      });
    }

    return globalHelper.jsonResponse(response, cams, 200);
  }

  async probe_camera() {
    return new Promise((resolve, reject) => {
      let cams = [];
      onvif.Discovery.probe();
      let timeout = setTimeout(() => {
        resolve(cams);
      }, 500);

      onvif.Discovery.on("device", function (cam) {
        cams.push({
          id: null,
          name: null,
          hostname: cam.hostname,
          username: null,
          password: null,
          port: cam.port,
        });

        clearTimeout(timeout);
        timeout = setTimeout(() => {
          resolve(cams);
        }, 500);
      });
    });
  }
}

module.exports = IpCameraController;
