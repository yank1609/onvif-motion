"use strict";
const globalHelper = use("Lib/global");
const History = use("App/Models/CameraHistory");
const moment = require("moment");
const Database = use("Database");

class ChartController {
  async index({ request, response }) {
    let query = request.all();
    let data;

    if (query.type == "daily") {
      data = await this.get_daily();
    } else if (query.type == "hourly") {
      data = await this.get_hourly(query.date);
    }

    return globalHelper.jsonResponse(response, data, 200);
  }

  async histories({ request, response }) {
    let query = request.all();
    let data = await History.query()
      .whereBetween(`activity_time`, [
        moment(query.date).format("Y-MM-DD HH:mm:ss"),
        moment(query.date).format("Y-MM-DD HH:59:59"),
      ])
      .with("ip_camera")
      .fetch();
    return globalHelper.jsonResponse(response, data, 200);
  }

  async get_daily() {
    let data = [];
    let labels = [];

    let prevDate = moment().subtract(6, "days");
    let currentDate = moment();

    prevDate = moment(prevDate._d).format("MM-DD-YYYY");
    currentDate = moment(currentDate._d).format("MM-DD-YYYY");

    let start = new Date(prevDate);
    let end = new Date(currentDate);

    while (start <= end) {
      let tmp = await History.query()
        .where(
          Database.raw(`DATE(activity_time)`),
          moment(start).format("Y-MM-DD")
        )
        .getCount();

      data.push(tmp);
      labels.push(moment(start).format("Y-MM-DD"));

      let newDate = start.setDate(start.getDate() + 1);
      start = new Date(newDate);
    }

    return {
      labels,
      data,
    };
  }

  async get_hourly(date) {
    let data = [];
    let labels = [];

    let tmp = moment(date);

    let prevDate = moment(tmp._d).format("MM-DD-YYYY 00:00:00");
    let currentDate = moment(tmp._d).format("MM-DD-YYYY 23:00:00");

    let start = new Date(prevDate);
    let end = new Date(currentDate);

    while (start <= end) {
      let tmp = await History.query()
        .whereBetween(Database.raw(`activity_time`), [
          moment(start).format("Y-MM-DD HH:mm:ss"),
          moment(start).format("Y-MM-DD HH:59:59"),
        ])
        .getCount();

      data.push(tmp);
      labels.push(
        `${moment(start).format("HH:mm")} - ${moment(start).format("HH:59")}`
      );

      let newDate = start.setHours(start.getHours() + 1);
      start = new Date(newDate);
    }

    return {
      labels,
      data,
    };
  }
}

module.exports = ChartController;
