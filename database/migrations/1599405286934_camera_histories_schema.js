"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class IpCamerasSchema extends Schema {
  up() {
    this.table("camera_histories", (table) => {
      table.string("indications");
    });
  }

  down() {
    this.table("camera_histories", (table) => {
      table.dropColumn("indications");
    });
  }
}

module.exports = IpCamerasSchema;
