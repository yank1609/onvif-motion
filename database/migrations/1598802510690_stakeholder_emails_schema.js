"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class StakeholderEmailsSchema extends Schema {
  up() {
    this.create("stakeholder_emails", (table) => {
      table.increments();
      table.timestamps();

      table.string("email").notNullable();
      table.string("name").notNullable();
    });
  }

  down() {
    this.drop("stakeholder_emails");
  }
}

module.exports = StakeholderEmailsSchema;
