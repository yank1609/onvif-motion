"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class IpCamerasSchema extends Schema {
  up() {
    this.table("ip_cameras", (table) => {
      // alter table
      table.dropColumn("ip");
      table.string("hostname").notNullable();
    });
  }

  down() {
    this.table("ip_cameras", (table) => {
      // reverse alternations
      table.dropColumn("hostname");
      table.string("ip").notNullable();
    });
  }
}

module.exports = IpCamerasSchema;
