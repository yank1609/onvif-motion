"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class TelegramsSchema extends Schema {
  up() {
    this.create("telegrams", (table) => {
      table.timestamps();
      table.bigInteger("id").unsigned().primary();
      table.string("name").notNullable();
    });
  }

  down() {
    this.drop("telegrams");
  }
}

module.exports = TelegramsSchema;
