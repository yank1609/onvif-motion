"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CameraHistoriesSchema extends Schema {
  up() {
    this.create("camera_histories", (table) => {
      table.increments();
      table.timestamps();

      table
        .integer("ip_camera_id")
        .unsigned()
        .references("id")
        .inTable("ip_cameras");
      table.json("images").notNullable();
      table.datetime("activity_time").notNullable();
    });
  }

  down() {
    this.drop("camera_histories");
  }
}

module.exports = CameraHistoriesSchema;
