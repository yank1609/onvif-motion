"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class IpCamerasSchema extends Schema {
  up() {
    this.create("ip_cameras", (table) => {
      table.increments();
      table.timestamps();

      table.string("ip").notNullable();
      table.string("username").notNullable();
      table.string("password").notNullable();
      table.string("port").notNullable();
      table.string("name").notNullable();
    });
  }

  down() {
    this.drop("ip_cameras");
  }
}

module.exports = IpCamerasSchema;
