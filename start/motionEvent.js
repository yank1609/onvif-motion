const Event = use("Event");
const Mail = use("Mail");
const Env = use("Env");
const CameraHistory = use("App/Models/CameraHistory");
const IpCamera = use("App/Models/IpCamera");
const Telegram = use("App/Models/Telegram");
const StakeholderEmail = use("App/Models/StakeholderEmail");
const moment = require("moment");
const Helpers = use("Helpers");
const fs = require("fs");
const TG = require("telegram-bot-api");

require("@tensorflow/tfjs-backend-cpu");
require("@tensorflow/tfjs-backend-webgl");
const tf = require("@tensorflow/tfjs-node");
const cocoSsd = require("@tensorflow-models/coco-ssd");
const { start } = require("repl");

Event.on("motion::save", async (data) => {
  let ip_camera = await IpCamera.find(data.ip_camera_id);
  let images = [0, 1, 2].map((i) => `${data.images}-${i}.jpeg`);

  let camera_history = await CameraHistory.create({
    ip_camera_id: ip_camera.id,
    activity_time: moment(parseInt(data.timestamp)).format("Y-MM-DD HH:mm:ss"),
    images: JSON.stringify(images),
  });

  if (Env.get("DETECT_OBJECT", "false") == "true") {
    Event.fire("motion:detect_object", {
      images,
      history: camera_history,
      ip_camera: ip_camera.toJSON(),
      camera: ip_camera.toJSON(),
    });
  } else {
    Event.fire("motion::send_email", {
      images,
      camera: ip_camera.toJSON(),
      history: camera_history.toJSON(),
      ip_camera: ip_camera.toJSON(),
    });
  }
});

Event.on(
  "motion:detect_object",
  async ({ images, history, camera, ip_camera }) => {
    let results = [];
    let finalResults = {};
    let startTime = moment();
    let keyMap = {
      person: "Manusia",
      car: "Mobil",
      cow: "Sapi",
      horse: "Kambing",
    };

    for (let i = 0; i < images.length; i++) {
      let img = images[i];
      const imageBuffer = fs.readFileSync(Helpers.publicPath(img));
      const tensorFeature = tf.node.decodeImage(imageBuffer);

      // Load the model.
      const model = await cocoSsd.load();
      const predictions = await model.detect(tensorFeature);

      results.push(predictions);
    }

    for (let i = 0; i < results.length; i++) {
      let imgPredictions = results[i];
      let imgResult = {};
      for (
        let predictionIndex = 0;
        predictionIndex < imgPredictions.length;
        predictionIndex++
      ) {
        let prediction = imgPredictions[predictionIndex];

        if (!imgResult[prediction.class]) {
          imgResult[prediction.class] = 0;
        }

        imgResult[prediction.class] += 1;
      }

      for (let ii = 0; ii < Object.keys(imgResult).length; ii++) {
        let key = Object.keys(imgResult)[ii];
        let keyMapResult = keyMap[Object.keys(imgResult)[ii]]
          ? keyMap[Object.keys(imgResult)[ii]]
          : "Objek tidak diketahui";

        if (!finalResults[keyMapResult]) {
          finalResults[keyMapResult] = {
            result: [],
            average: 0,
          };
        }

        finalResults[keyMapResult]["result"].push(imgResult[key]);
      }
    }

    let textResults = "";
    for (let i = 0; i < Object.keys(finalResults).length; i++) {
      let key = Object.keys(finalResults)[i];
      finalResults[key].average =
        finalResults[key].result.reduce((a, b) => a + b, 0) /
        finalResults[key].result.length;

      if (i == 0) {
        textResults += `${Math.floor(finalResults[key].average)} ${key}`;
      } else if (i === Object.keys(finalResults).length - 1) {
        textResults += ` dan ${Math.floor(finalResults[key].average)} ${key}`;
      } else {
        textResults += ` , ${Math.floor(finalResults[key].average)} ${key}`;
      }
    }

    history.merge({
      indications: textResults,
    });
    await history.save();

    Event.fire("motion::send_email", {
      images,
      camera: ip_camera,
      history: history.toJSON(),
      ip_camera: ip_camera,
    });
  }
);

Event.on(
  "motion::send_email",
  async ({ images, camera, history, ip_camera }) => {
    let stakeholders = (await StakeholderEmail.all()).toJSON();
    await Mail.send(
      "ip_camera.emails.motion",
      {
        camera,
        history,
      },
      (message) => {
        stakeholders.forEach((stakeholder) => {
          message.to(stakeholder.email, stakeholder.name);
        });

        message
          .from(`Peringatan CCTV <${Env.get("MAIL_USERNAME")}>`)
          .subject(`Peringatan Pergerakan Kamera ${ip_camera.name}`);

        images.forEach((img) => {
          message.attach(Helpers.publicPath(img));
        });
      }
    );

    Event.fire("motion::send_telegram", { images, camera, history, ip_camera });
  }
);

Event.on(
  "motion::send_telegram",
  async ({ images, camera, history, ip_camera }) => {
    const api = new TG({ token: Env.get("TELEGRAM_TOKEN") });
    let ids = await Telegram.ids();
    let image_ids = [];

    ids.forEach((id) => {});

    for (let idx = 0; idx < ids.length; idx++) {
      let id = ids[idx];
      await api.sendMessage({
        chat_id: id,
        parse_mode: "MarkdownV2",
        text: `
*Peringatan Pergerakan Kamera ${ip_camera.name}*

Telah terjadi pergerakan di *${camera.name}* pada *${moment(
          history.activity_time
        ).format("DD MM Y HH:mm:ss")}* ${
          history.indications
            ? ", *Terindikasi " + history.indications + "*"
            : ""
        } 
              `,
      });

      for (let i = 0; i < images.length; i++) {
        let img = images[i];
        let imageFile = image_ids[i]
          ? image_ids[i]
          : fs.createReadStream(Helpers.publicPath(img));

        let response = await api.sendPhoto({
          chat_id: id,
          photo: imageFile,
          caption: `${ip_camera.name} ke ${i + 1}`,
        });
        image_ids[i] = response.photo[0].file_id;
      }
    }
  }
);
