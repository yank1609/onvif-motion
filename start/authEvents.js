const Config = use("Config");
const Event = use("Event");
const Mail = use("Mail");
const Env = use("Env");

Event.on("new::user", async (user) => {
  await Mail.send("auth.emails.welcome-mail", user, (message) => {
    message.to(user.email);
    message.from(Env.get("MAIL_USERNAME"));
  });
});

Event.on("forgot::password", async ({ user, token }) => {
  await Mail.send(
    "auth.emails.password",
    {
      token,
      user,
      appURL: Config.get("adonis-auth-scaffold.appURL"),
    },
    (message) => {
      message.to(user.email);
      message.from(Env.get("MAIL_USERNAME")).subject("Password reset");
    }
  );
});
