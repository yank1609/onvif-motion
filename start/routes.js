"use strict";

require("./authRoutes.js");

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");
const Config = use("Config");
const registrationSuccessRoute = Config.get(
  "adonis-auth-scaffold.registrationSuccessRedirectTo"
);

Route.get("/", ({ response, auth }) => {
  if (!auth.user) {
    return response.redirect("/login");
  }

  return response.redirect(`/auth/dashboard`);
});

Route.group(() => {
  Route.get("/", "IpCameraController.index")
    .as("ip_camera.index")
    .middleware(["auth"]);

  Route.post("/", "IpCameraController.create")
    .as("ip_camera.create")
    .middleware(["auth"]);

  Route.get("/probe", "IpCameraController.probe").as("ip_camera.probe");
}).prefix("ip_cameras");

Route.group(() => {
  Route.get("/", "StakeholderEmailController.index")
    .as("stakeholder_email.index")
    .middleware(["auth"]);

  Route.post("/", "StakeholderEmailController.create")
    .as("stakeholder_email.create")
    .middleware(["auth"]);
  Route.delete("/:id", "StakeholderEmailController.delete")
    .as("stakeholder_email.delete")
    .middleware(["auth"]);
}).prefix("stakeholder_emails");

Route.group(() => {
  Route.get("/", "ChartController.index").as("charts.index");
  Route.get("/histories", "ChartController.histories").as("charts.index");
}).prefix("charts");
